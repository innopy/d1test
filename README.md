
docker-compose up -d --build


Note: This Dockerfile is for development and testing only since it's using wipe-db everytime container restarts




### Kubernetes version
cd kubernetes

create configmap (1)

`kubectl apply -f .\env-configmap.yaml`

Create postgres instance (2)

`kubectl apply -f .\postgres-db-deployment.yaml`

Create node0 instance (3)

`kubectl apply -f .\node0-deployment.yaml`

Check service (4)

```
kubectl get svc
NAME          TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)             AGE
adminer       NodePort    10.99.119.171   <none>        8089:32089/TCP      22m
kubernetes    ClusterIP   10.96.0.1       <none>        443/TCP             18d
node0         ClusterIP   10.101.39.24    <none>        7740/TCP,9870/TCP   26m
postgres-db   ClusterIP   10.106.102.63   <none>        5432/TCP            26m
```

Using Kubernetes service so that Kube pod can communicate other services

Start node1

`kubectl apply -f .\node1-deployment.yaml`

Start node2

`kubectl apply -f .\node2-deployment.yaml`


Check service

```
kubectl get svc
NAME          TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)             AGE
adminer       NodePort    10.99.119.171   <none>        8089:32089/TCP      22m
kubernetes    ClusterIP   10.96.0.1       <none>        443/TCP             18d
node0         ClusterIP   10.101.39.24    <none>        7740/TCP,9870/TCP   26m
node1         ClusterIP   10.98.126.89    <none>        7740/TCP,9870/TCP   3m25s
node2         ClusterIP   10.101.167.32   <none>        7740/TCP            12m
postgres-db   ClusterIP   10.106.102.63   <none>        5432/TCP            26m

```


### References

(1) Understanding Configmap 

https://www.katacoda.com/courses/kubernetes/managing-secrets

(2)(3) Understanding Deployment

https://www.katacoda.com/courses/kubernetes/creating-kubernetes-yaml-definitions

(4) Service commnunication

https://www.katacoda.com/courses/kubernetes/networking-introduction
